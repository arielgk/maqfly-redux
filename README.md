


Adding new components


Create Part in src/components/Preview/parts/


declare the new part component in src/components/Preview/item.js




TODO :
- Decoupling Flyer Settings:

    Should settings be in a separated Reducer?
    pros:
        -faster access to settings
        -better state tree according redux recommendations
        -own actions
    cons:
        there isn't a dedicated Component for Flyer settings and code readability could be be worse.


    how:
        on single flyer mounting, add flyer.settings to state.settings.
        create settingsAction file and export function to save and get settings.
        create settingsReducer
        replace props from Options/index.js to be feeded with state.settings instead of ownProps.settings .

Done:


