import React from 'react';
// import Item from '../Item'

import DragAndDrop from '../DragAndDrop';

const MaqFly = ({items, preview, activeItem}) => {
    return (
        <div>
            <table width={599} border={0} bgcolor="#F6F6F6" cellSpacing={0} cellPadding={0} align="center">
                <tbody>
                <tr>
                    <td colSpan={7}>
                        <table width={599} style={{
                            textAlign: 'center',
                            fontFamily: 'Helvetica, Arial, sans-serif',
                            fontSize: 13,
                            background: '#fff'
                        }}>
                            <tbody>
                            <tr>
                                <td colSpan={7}>
                                    {/*<a*/}
                                    {/*href="https://mailchi.mp/83aac08a8fcc/jornada-big-data-y-anlisis-moderno-de-redes-sociales?e=0138603334"*/}
                                    {/*style={{color: '#6cf', fontSize: 12}}>Ver en mi navegador</a>*/}
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table width={599} border={0} bgcolor="#F6F6F6" cellSpacing={0} cellPadding={0} align="center">
                <tbody>
                <tr>
                    <td colSpan={7} bgcolor="#25AAE1"><img
                        src="http://untref.edu.ar/flyers/img/overlay_portada_Big_Data_y_analisis_moderno_de_redes_sociales.png?v=5b2d5c774506d"
                        width={599} height={339} alt="UNTREF Novedades" style={{
                        display: 'block',
                        border: 'none',
                        outline: 'none',
                        fontFamily: 'Arial, Helvetica, sans-serif',
                        background: '#25aae1',
                        color: '#FFF',
                        fontSize: 24
                    }} align="top" border={0}/></td>
                </tr>
                <tr height={20}/>
                <tr>
                    <td width={39} rowSpan={6}>&nbsp;</td>
                    <td colSpan={6} bgcolor=""
                        style={{fontFamily: 'Arial, Helvetica, sans-serif', paddingTop: 10, paddingBottom: 60}}>
                        <DragAndDrop items={items} preview={preview} activeItem={activeItem}/>
                    </td>

                    <td width={49} rowSpan={5}>&nbsp;</td>
                </tr>


                <tr>
                    <td colSpan={6} bgcolor=""
                        style={{fontFamily: 'Arial, Helvetica, sans-serif', paddingTop: 30, paddingBottom: 60}}>

                    </td>
                </tr>
                <tr/>
                <tr>
                    <td colSpan={5} style={{fontFamily: 'Arial, Helvetica, sans-serif', fontSize: 16, color: '#000'}}/>
                </tr>
                <tr>
                    <td colSpan={7}/>
                </tr>
                <tr>
                    <td height={97}>&nbsp;</td>
                    <td width={274} align="left" valign="top"><img
                        src="http://untref.edu.ar/mailing_untref/novedades/images/logo-utf-new2.jpg" width={246}
                        height={57} alt="Universidad Nacional de Tres de Febrero"
                        style={{fontFamily: 'Arial, Helvetica, sans-serif', color: '#0f4665', fontSize: 20}} align="top"
                        border={0}/></td>
                    <td width={4} align="left" valign="top">&nbsp;</td>
                    <td width={27} align="left" valign="top">&nbsp;</td>
                    <td width={95} align="center" valign="top"><a href="http://untref.edu.ar" target="_blank" rel="noopener noreferrer"><img
                        src="http://untref.edu.ar/mailing_untref/images/untrefeduar2016.png" width={95} height={50}
                        align="top" border={0} alt={'redes-0'}/></a></td>
                    <td width={111} align="left" valign="top">
                        <table width={50} height={97} border={0} cellSpacing={0} cellPadding={0}>
                            <tbody>
                            <tr>
                                <td align="left" valign="top"><a href="https://twitter.com/untref" target="_blank" rel="noopener noreferrer"><img
                                    src="http://untref.edu.ar/mailing_untref/images/twitter2016.png" width={30}
                                    height={50} align="top" border={0}  alt={'redes-1'}/></a></td>
                                <td align="left" valign="top"><a href="https://facebook.com/untref" target="_blank" rel="noopener noreferrer"><img
                                    src="http://untref.edu.ar/mailing_untref/images/facebook2016.png" width={21}
                                    height={50} align="top" border={0}  alt={'redes-2'}/></a></td>
                                <td align="left" valign="top"><a href="https://instagram.com/untref/"
                                                                 target="_blank" rel="noopener noreferrer"><img
                                    src="http://untref.edu.ar/mailing_untref/images/instagram2016.png" width={30}
                                    height={50} align="top" border={0}  alt={'redes-3'}/></a></td>
                                <td align="left" valign="top"><a href="https://www.linkedin.com/edu/school?id=10153&trk"
                                                                 target="_blank" rel="noopener noreferrer"><img
                                    src="http://untref.edu.ar/mailing_untref/images/linkedin2016.png" width={30}
                                    height={50} align="top" border={0}  alt={'redes-4'}/></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    )

}

export default MaqFly;