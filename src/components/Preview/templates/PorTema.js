import React from 'react';
// import Item from '../Item'

import DragAndDrop from '../DragAndDrop';

const PorTema = ({items, preview, activeItem, settings}) => {
    return (<table style={{
            width: 700,
            border: 0,
            cellSpacing: 0,
            cellPadding: 0,
            align: 'center',
            fontFamily: 'Arial, san-serif',
            color: '#262424',
            fontSize: 16
        }}>
        <tr>
            <td>
                <img src="images/logo.jpg" width="104" height="408" alt="UNTREF">
            </td>
            <td>
                <img src="images/img-top.jpg" width="596" height="408" alt="UNTREF">
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 34,
                paddingLeft: 30,
                paddingRight: 60
            }}>
            <span style={{
                fontSize: 17
            }}><b>Charlas</b></span>
                <h1 style={{
                    fontSize: 30,
                    marginTop: 18,
                    marginBottom: 25
                }}>La crisis griega, el MIR chileno y los 68</h1>
                <p style={{
                    textAlign: 'justify',
                    hyphens: 'auto'
                }}>Los encuentros académicos se refieren a tres procesos internacionales analizados en clave histórica y
                    actual por una especialista de las circulaciones revolucionarias en América Latina y el
                    Mediterráneo.</p>
                <p style={{
                    marginTop: 30
                }}><b>Expositora:</b> Dra. Eugenia Palieraki (Université de Cergy-Pontoise, Francia)</p>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 50,
                paddingLeft: 30,
                paddingRight: 60
            }}>
                <h2 style={{
                    fontSize: 21,
                    color: '#406290',
                }}>La crisis griega: perspectivas históricas y coyuntura actual</h2>
                <span style={{
                    fontSize: 17,
                    marginTop: 8,
                    display: 'block'
                }}><b>Viernes 14 de septiembre, de 18:00 a 20:00 hs.</b></span>
                <a href="#" style={{
                    background: '#66A4D3',
                    fontWeight: 'bold',
                    fontSize: 15,
                    color: 'white',
                    textDecoration: 'none',
                    padding: '5px 10px',
                    marginTop: 22,
                    display: 'table'
                }}>Ver más</a>
                <hr style={{
                    align: 'left',
                    border: 'none',
                    height: '0.5px',
                    width: 142,
                    background: '#66A4D3',
                    marginTop: 17,
                    marginBottom: 28
                }}>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 0,
                paddingLeft: 30,
                paddingRight: 60
            }}>
                <h2 style={{
                    fontSize: 21,
                    color: '#406290'
                }}>La crisis griega: perspectivas históricas y coyuntura actual</h2>
                <span style={{
                    fontSize: 17,
                    marginTop: 8,
                    display: 'block'
                }}><b>Viernes 14 de septiembre, de 18:00 a 20:00 hs.</b></span>
                <a href="#" style={{
                    background: '#66A4D3',
                    fontWeight: 'bold',
                    fontSize: 15,
                    color: 'white',
                    textDecoration: 'none',
                    padding: '5px 10px',
                    marginTop: 22,
                    display: 'table'
                }}>Ver más</a>
                <hr style={{
                    align: 'left',
                    border: 'none',
                    height: '0.5px',
                    width: 142,
                    background: '#66A4D3',
                    marginTop: 17,
                    marginBottom: 28,
                }}>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 0,
                paddingLeft: 30,
                paddingRight: 60
            }}>
                <h2 style={{
                    fontSize: 21,
                    color: '#406290'
                }}>La crisis griega: perspectivas históricas y coyuntura actual</h2>
                <span style={{
                    fontSize: 17,
                    marginTop: 8,
                    display: 'block'
                }}><b>Viernes 14 de septiembre, de 18:00 a 20:00 hs.</b></span>
                <a href="#" style={{
                    background: '#66A4D3',
                    fontWeight: 'bold',
                    fontSize: 15,
                    color: 'white',
                    textDecoration: 'none',
                    padding: '5px 10px',
                    marginTop: 22,
                    display: 'table'
                }}>Ver más</a>
                <hr style={{
                    align: 'left',
                    border: 'none',
                    height: 0.5,
                    width: 142,
                    background: '#66A4D3',
                    marginTop: 17,
                    marginBottom: 28
                }}>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 14,
                paddingLeft: 30,
                paddingRight: 60
            }}>
                <p style={{
                    fontSize: 20
                }}>-</p>
                <p style={{
                    marginBottom: 30
                }}><b>Contacto:</b> ieh@untref.edu.ar</p>
                <p style={{
                    marginBottom: 30
                }}>Actividades no aranceladas</p>
                <p><b>Organiza:</b> Instituto de Estudios Históricos</p>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 60,
                paddingLeft: 30,
                paddingRight: 0'
            }}>
                <table style={{
                    width: 566,
                    border: 0,
                    cellSpacing: 0,
                    cellPadding: 0,
                    align: 'center'
                }}>
                    <tr>
                        <td>
                            <a href="http://untref.edu.ar/" target="_blank">
                                <img src="images/untref.jpg" width="118" height="36" alt="UNTREF">
                            </a>
                        </td>
                        <td>
                            <a href="https://twitter.com/untref" target="_blank">
                                <img src="images/twitter.jpg" width="35" height="36" alt="Twitter">
                            </a>
                        </td>
                        <td>
                            <a href="https://www.facebook.com/untref" target="_blank">
                                <img src="images/facebook.jpg" width="25" height="36" alt="Facebook">
                            </a>
                        </td>
                        <td>
                            <a href="https://www.instagram.com/untref/" target="_blank">
                                <img src="images/instagram.jpg" width="40" height="36" alt="Instagram">
                            </a>
                        </td>
                        <td>
                            <a href="https://www.linkedin.com/edu/school?id=10153&trk=feed-actor-image" target="_blank">
                                <img src="images/linkedin.jpg" width="23" height="36" alt="Linkedin">
                            </a>
                        </td>
                        <td style={{
                            width: 102
                        }}>

                        </td>
                        <td>
                            <a href="http://untref.edu.ar/" target="_blank">
                                <img src="images/logo-footer.jpg" width="126" height="36" alt="UNTREF">
                            </a>
                        </td>
                        <td style={{
                            width: 104
                        }}>

                        </td>

                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td style={{
                paddingTop: 34
            }}>
                <img src="images/trama.jpg" width="596" height="71" alt="">
            </td>
        </tr>


    </table>
    )

}

export default PorTema;