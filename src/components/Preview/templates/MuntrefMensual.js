import React from 'react';
// import Item from '../Item'

import DragAndDrop from '../DragAndDrop';

const MuntrefMensual = ({items, preview, activeItem, settings}) => {
    return (<table style={{
            width: 525,
            border: 0,
            cellSpacing: 0,
            cellPadding: 0,
            align: 'center',
            fontFamily: 'Arial, san-serif',
            color: '#211915',
            fontSize: 14
        }}>
        <tr>
            <td>
                <table style={{
                    width: 525,
                    border: 0,
                    cellSpacing: 0,
                    cellPadding: 0,
                    align:'center'
                }}>
                    <tr>
                        <td>
                            <a href="http://untref.edu.ar/muntref/" target="_blank">
                                <img src="images/logo-muntref.jpg" width="281" height="100" alt="MUNTREF">
                            </a>
                        </td>
                        <td valign="top" style={{
                            paddingTop: 18
                        }}>
                            <b style={{
                           fontSize: 22,
                           color: '#ABABAB'
                            }}>Agenda</b><br>
                            <b style={{
                           fontSize: 22,
                           color: '#ABABAB'
                            }}>15.09 al 30.09.18</b><br>
                            <b style={{
                           fontSize: 17,
                           color: '#ABABAB',
                           borderTop: 'solid thin #ABABAB',
                           paddingTop: 20,
                           marginTop: 8,
                           display: 'block'
                            }}>Actividades gratuitas</b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style={{
            height: 44
        }}>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <img src="images/header-cac.jpg" width="525" height="75" alt="MUNTREF | Centro de Arte Contemporáneo">
            </td>
        </tr>
        <tr>
            <td style={{
                paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-cac.jpg" width="525" height="349" alt="MUNTREF | Centro de Arte Contemporáneo">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
                paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#21191',
                    textDecoration: 'none'
                }}>
                <span style={{
                    fontSize: 13
                }}>Taller para chicos</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}>Sonidos viajeros en primavera</h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>Recorremos a través de cuentos y canciones las historias de “Giuseppe”, un inmigrante italiano. Leemos,
                        cantamos y bailamos. Al finalizar cada uno podrá crear su propio instrumento de viaje.</p>
                    <p style={{
               marginTop: 18,
               textDecoration: 'underline'
                    }}><b>» Domingo 30 a las 15:00 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>
        <tr>
            <td style={{
                paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-cac2.jpg" width="525" height="349"
                         alt="MUNTREF | Centro de Arte Contemporáneo">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#211915',
                    textDecoration: 'none'
                }}>
                <span style={{
                  fontSize: 13
                }}>Actividad participativa</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}>Visita musical</h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>Invitamos a jóvenes y adultos a recorrer la historia de la inmigración en nuestro país a través de músicas
                        típicas.</p>
                    <p style={{
               marginTop: 18,
               textDecoration: 'underline'
                    }}><b>» Domingo 30 a las 16:00 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>

        <tr>
            <td>
                <img src="images/header-mav.jpg" width="525" height="75" alt="MUNTREF | Museo de Artes Visuales">
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-mav.gif" width="525" height="349" alt="MUNTREF | Museo de Artes Visuales">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#211915',
                    textDecoration: 'none'
                }}>
                <span style={{
                  fontSize: 13
                }}>Taller a cargo de Catalina León</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}><em>¿Qué hago en este hogar llamado tierra?</em> Diálogos entre las Casas IV, X y XI.</h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>La instalación, que propone repensar las representaciones de los signos del zodíaco, será el escenario
                        para pensar cómo se articula en cada uno de los visitantes la relación entre las casas
                        astrológicas IV (el hogar, las raíces), X (la profesión, la sociedad ) y XI (la comunidad y los
                        futuros posibles).</p>
                    <p style={{
               marginTop: 18,
               textDecoration: 'underline'
                    }}><b>» Sábado 15 a las 16:00 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>

        <tr>
            <td>
                <img src="images/header-can.jpg" width="525" height="77" alt="MUNTREF | Centro de Arte y Naturaleza">
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-can.jpg" width="525" height="346" alt="MUNTREF | Centro de Arte y Naturaleza">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#211915',
                    textDecoration: 'none'
                }}>
                <span style={{
                  fontSize: 13
                }}>Taller para chicos</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}>¡Recibimos la primavera jugando!</h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>Realizaremos un recorrido participativo por la muestra <em>Zoología Fantástica</em>, de Pablo La Padula, y
                        llevaremos a cabo un taller de arte para conocer todos los secretos del artista.</p>
                    <p style={{
               marginTop: 18,
               textDecoration: 'underline'
                    }}><b>» Domingo 23 a las 17:00 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-can2.jpg" width="525" height="346" alt="MUNTREF | Centro de Arte y Naturaleza">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#211915',
                    textDecoration: 'none'
                }}>
                <span style={{
                  fontSize: 13
                }}>Lectura / performance / intervención</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}>Relatos apócrifos (de monstruos, bichos y seres que tal vez existieron)</h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>Performance a cargo de Mariela Yeregui – Directora de la Maestría en Tecnología y Estética de las Artes
                        Electrónicas.</p>
                    <p style={{
               marginTop: 18,
               text-decoration: 'underline'
                    }}><b>» Domingo 30 a las 17:00 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>

        <tr>
            <td>
                <img src="images/header-cayc.jpg" width="525" height="78" alt="MUNTREF | Centro de Arte y Naturaleza">
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank">
                    <img src="images/img-cayc.jpg" width="525" height="355" alt="MUNTREF | Centro de Arte y Naturaleza">
                </a>
            </td>
        </tr>
        <tr>
            <td style={{
            paddingTop: 25
            }}>
                <a href="#" target="_blank" style={{
                    color: '#211915',
                    textDecoration: 'none'
                }}>
                <span style={{
                  fontSize: 13
                }}>Charlas</span>
                    <h1 style={{
                fontSize: 19,
                fontStyle: 'italic',
                marginTop: 10,
                marginBottom: 8
                    }}><em>De la periferia al centro: Arte Electrónico en los bordes</em></h1>
                    <p style={{
               textAlign: 'justify',
               hyphens: 'auto'
                    }}>A cargo del grupo HORMIGUERO.</p>
                    <p style={{
               marginTop: 18,
               textDecoration: 'underline'
                    }}><b>» Jueves 27 a las 12:30 hs.</b></p>
                </a>
            </td>
        </tr>
        <tr style={{
            height: 37
        }}>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <a href="#" style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        textDecoration: 'none',
                        color: '#FFFFFF',
                        background: '#000000',
                        paddingTop: 9,
                        paddingLeft: 6,
                        paddingRight: 6,
                        paddingBottom: 9,
                        marginTop: 15,
                        display: 'table'
                }}>
                    Ver todas las muestras actuales »
                </a>
            </td>
        </tr>

        <tr>
            <td style={{
            marginTop: 52,
            marginBottom: 15,
            width: 12,
            height: 1,
            background: '#000000',
            display: 'table'
            }}>

            </td>
        </tr>

        <tr>
            <td>
                <table style={{
                    width: 525,
                    border: 0,
                    cellSpacing: 0,
                    cellPadding: 0,
                    align: 'center',
                    fontSize: 10,
                    paddingBottom: 38
                }}>
                    <tr>
                        <td style={{
                            width: 235,
                            verticalAlign: 'top'
                        }}>
                            <b>MUNTREF CENTRO DE ARTE Y CIENCIA</b><br>
                            <p>TECNÓPOLIS. Parque Centenario. Villa Martelli.<br>
                                Accesos por Av. General Paz, Zufriategui y Av. de los Constituyentes.</p>
                        </td>
                        <td style={{
                            width: 45
                        }}>

                        </td>
                        <td style={{
                            width: 235,
                            verticalAlign: 'top'
                        }}>
                            <b>MUNTREF CENTRO DE ARTE Y NATURALEZA</b><br>
                            <p>Sede Ecoparque (Ex zoológico).<br>
                                Av. Sarmiento 2725, Palermo. CABA.</p>
                        </td>
                    </tr>

                    <tr>
                        <td style={{
                            height: 28
                        }}>

                        </td>
                    </tr>

                    <tr>
                        <td style={{
                            width: 235,
                            verticalAlign: 'top'
                        }}>
                            <b>MUNTREF MUSEO DE ARTES VISUALES</b><br>
                            <p>Sede Caseros I. Valentín Gómez 4838, Caseros.<br>
                                4759-0040/3528/3537 int. 115.</p>
                        </td>
                        <td style={{
                            width: 45
                        }}>

                        </td>
                        <td style={{
                            width: 235,
                            verticalAlign: 'top'
                        }}>
                            <b>MUNTREF CENTRO DE ARTE CONTEMPORÁNEO Y MUSEO DE LA INMIGRACIÓN</b><br>
                            <p>Av. Antártida Argentina (entre Dirección Nacional de Migraciones y Buquebus), Puerto
                                Madero. CABA. 4893-0322.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style={{
                height: 3,
                background: '#000000'
            }}>

            </td>
        </tr>

        <tr>
            <td style={{
            paddingTop: 20
                    }}>
                <table style={{
                    width: 525,
                    border: 0,
                    cellSpacing: 0,
                    cellPadding: 0,
                    align: 'center'
                }}>
                    <tr>
                        <td style={{
                            verticalAlign: 'top'
                        }}>
                            <table style={{
                                width: 118,
                                border: 0,
                                cellSpacing: 0,
                                cellPadding: 0,
                                align: 'left'
                            }}>
                                <tr>
                                    <td>
                                        <a href="http://untref.edu.ar/muntref/" target="_blank">
                                            <img src="images/url-muntref.jpg" width="118" height="12" alt="MUNTREF">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style={{
                                            width: 80,
                                            border: 0,
                                            cellSpacing: 0,
                                            cellPadding: 0,
                                            align: 'left'
                                        }}>
                                            <tr>
                                                <td style={{
                                                    align: 'left',
                                                    width: 35
                                                }}>
                                                    <a href="https://twitter.com/MUNTREF" target="_blank">
                                                        <img src="images/twitter.jpg" width="35" height="36"
                                                             alt="Twitter">
                                                    </a>
                                                </td>
                                                <td style={{
                                                    align: 'left',
                                                    width: 25
                                                }}>
                                                    <a href="https://www.facebook.com/muntref" target="_blank">
                                                        <img src="images/facebook.jpg" width="25" height="36"
                                                             alt="Facebook">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style={{
                            verticalAlign: 'top'
                        }}>
                            <table style={{
                                width: 86,
                                border: 0,
                                cellSpacing: 0,
                                cellPadding: 0,
                                align: 'left'
                            }}>
                                <tr>
                                    <td>
                                        <a href="http://untref.edu.ar/" target="_blank">
                                            <img src="images/untref2.jpg" width="70" height="13" alt="UNTREF">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style={{
                                            width: 123,
                                            border: 0,
                                            cellSpacing: 0,
                                            cellPadding: 0
                                            align: 'left'
                                        }}>
                                            <tr>
                                                <td>
                                                    <a href="https://twitter.com/untref" target="_blank">
                                                        <img src="images/twitter.jpg" width="35" height="36"
                                                             alt="Twitter">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://www.facebook.com/untref" target="_blank">
                                                        <img src="images/facebook.jpg" width="25" height="36"
                                                             alt="Facebook">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://www.instagram.com/untref/" target="_blank">
                                                        <img src="images/instagram.jpg" width="40" height="36"
                                                             alt="Instagram">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://www.linkedin.com/edu/school?id=10153&trk=feed-actor-image"
                                                       target="_blank">
                                                        <img src="images/linkedin.jpg" width="23" height="36"
                                                             alt="Linkedin">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>

                        </td>
                        <td style={{
                            align: 'right',
                            verticalAlign:'top',
                            marginTop: 5,
                            display: 'block'
                        }}>
                            <a href="http://untref.edu.ar/" target="_blank">
                                <img src="images/logo-footer2.jpg" width="195" height="22" alt="UNTREF">
                            </a>
                        </td>
                    </tr>
                    <tr>


                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style={{
                paddingTop: 34
                    }}>
                <!--<img src="images/trama.jpg" width="596" height="71" alt="">-->
            </td>
        </tr>


    </table>
    )

}

export default MuntrefMensual;