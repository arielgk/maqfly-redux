import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as itemsActions from "../../actions/itemsActions";
import {IoIosTrash } from 'react-icons/io';
import {IoIosHammer } from 'react-icons/io';

const Edit = ({index, actions, item}) => {

    const setActiveItem = (index) => {
        actions.setActiveItem(index)
    }
    const deleteItem = (index, item) => {

        let r = window.confirm("Sure?");
        if (r === true) {
            actions.deleteItem(index, item.id);
        }

    }

    const styles = {
        position: 'absolute',
        top: 0,
        right: 0,
        height: '100%',
        width: '100%',
        background: 'rgba(0,0,0,.3)',
        zIndex:10,

    }
    const tools = {
        // display: 'flex',
        // justifyContent: 'flex-end',
        // alignItems: 'center',

    }

    const toolButton = {

        // background: 'rgba(0,0,0,.6)',
        // padding:'3px',
        // fontSize:20,
        // display:'flex',
        // justifyContent:'center',
        // alignItems:'center',

    }

    const ButtonStyle = {

        // color: 'white',
        // textAlign: 'center',
        // cursor: 'pointer',


    };


    return (
        <div style={styles}>
            <div style={tools} className={'tools-container'}>
                <div style={toolButton} className={'tool-container'}>
                    <div style={ButtonStyle} className={'tool-icon-container'} onClick={() => {
                        setActiveItem(item.id)
                    }}>
                        <IoIosHammer/>
                    </div>
                </div>
                <div style={toolButton} className={'tool-container'}>
                    <div style={ButtonStyle} className={'tool-icon-container'} onClick={() => {
                        deleteItem(index, item)
                    }}>
                        <IoIosTrash/>
                    </div>
                </div>


            </div>
        </div>
    )

}

Edit.propTypes = {

    actions: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {}
}
const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Edit)