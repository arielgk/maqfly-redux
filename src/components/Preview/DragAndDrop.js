import React from 'react';
import Item from './Item'
import PropTypes from 'prop-types';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd'
import { connect } from 'react-redux';
import * as itemsActions from "../../actions/itemsActions";
import {bindActionCreators} from "redux";

const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: 'none',
    background: isDragging ? 'lightgreen' : 'transparent',
    ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'transparent',
});

class DragAndDrop extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            items: props.items,
        };
        this.onDragEnd = this.onDragEnd.bind(this);

    }

    reorder = (list, startIndex, endIndex) => {

        // all this must sync be with redux store
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);

        this.props.actions.orderItems(startIndex,endIndex);

        return result;


    };

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const items = this.reorder(
            this.state.items,
            result.source.index,
            result.destination.index
        );

        this.setState({
            items,
        });
        this.props.actions.saveOrderItems(items);

    }

    componentWillReceiveProps(props) {
        //isolate new item and add it to bottom
        this.setState({
            items: props.items
        });

    }

    render() {
        return (<DragDropContext onDragEnd={this.onDragEnd}>
            <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        style={getListStyle(snapshot.isDraggingOver)}
                    >
                        {this.state.items.map((item, index) => (
                            <Draggable key={'test-' + index} draggableId={'item-' + index} index={index}>
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={getItemStyle(
                                            snapshot.isDragging,
                                            provided.draggableProps.style
                                        )}
                                    >
                                        <Item key={'item-' + index} item={item} preview={this.props.preview}
                                              index={index} activeItem={this.props.activeItem}/>

                                    </div>
                                )}
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>)
    }
}


DragAndDrop.propTypes={
    actions:PropTypes.object.isRequired,
};


const mapStateToProps = (state,ownProps)=>{
    return{

    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions}, dispatch)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(DragAndDrop)