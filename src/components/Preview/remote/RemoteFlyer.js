import React from 'react';
import PropTypes from 'prop-types';

import RemoteTemplate from '../remote/RemoteTemplate';
import {connect} from 'react-redux';
import * as actions from "../../../actions/flyerActions";


import {bindActionCreators} from "redux";

class RemoteFlyer extends React.Component {


    componentDidMount() {
        this.props.actions.getFlyer(this.props.match.params.id)
    }

    componentDidUpdate(nextProps) {
        // if(this.props !== nextProps){
        //     this.props.actions.getFlyer(this.props.match.params.id, this.props.opt)
        // }

    }


    render() {



        const {flyer, settings} = this.props;
        return (
            <div>



            {flyer ?
            <div className="container">

                        <div>

                            <RemoteTemplate flyer= {flyer}  preview={true} settings={settings}/>
                        </div>





            </div>
                    : ''}
            </div>
        )

    }
}

RemoteFlyer.propTypes = {
    actions: PropTypes.object.isRequired,
    flyers: PropTypes.array.isRequired,
    settings:PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    console.log(state);
    let flyer = state.flyers.filter((i, index) => {
        return i.id === parseInt(ownProps.match.params.id, 10);
    })





    return {

        flyer: flyer.length > 0 ? flyer[0]: null ,
        flyers:state.flyers,
        settings:state.settings,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(RemoteFlyer)