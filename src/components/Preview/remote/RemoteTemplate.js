import React from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from "../../../actions/itemsActions";
import {bindActionCreators} from "redux";

class RemoteTemplate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sourceCode: '',
            showCode: false,
        }

        this.showCode = this.showCode.bind(this);
    }

    getHtml() {
        const RE_HTML_COMMENTS = /<!--[\s\S]*?-->/g;
        const html = this.fenceView.innerHTML.replace(RE_HTML_COMMENTS, '');

        this.setState({
            sourceCode: html,
        })
    }

    componentDidMount() {
        this.getHtml();
        this.props.actions.listItems(this.props.flyer.id)

    }

    componentDidUpdate(nextProps) {
        if (this.props !== nextProps) {
            this.getHtml();
        }
    }

    showCode() {
        this.setState({
            showCode: !this.state.showCode,
        })
    }

    render() {

        const {items,  flyer, templates,settings} = this.props;
        const TagName = templates.filter((template) => {
            return template.name === flyer.template
        })[0].component;

        return (

            <div className="preview-container" style={{width: '100%'}}>

                <div className="preview"
                     style={{display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%'}}>


                    <TagName preview={false} items={items} settings={settings}/>


                </div>

                <div className="source-container" style={{display: 'none'}} ref={(n) => (this.fenceView = n)}>
                    <TagName preview={false} items={items} settings={settings}/>
                </div>


            </div>

        )
    }

}

RemoteTemplate.propTypes = {

    flyer: PropTypes.object.isRequired,
    templates: PropTypes.array.isRequired,

    items: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    settings:PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    return {


        flyer: ownProps.flyer,
        templates: state.templates,

        items: state.items,
        settings:state.settings,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RemoteTemplate);