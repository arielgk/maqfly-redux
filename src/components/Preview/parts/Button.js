import React from 'react';
import Edit from '../Edit';

import *  as defaultStyles from '../../DefaultStyles';





class Button extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })
    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })
    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;
        const title = compo.options[0].value;
        const link = compo.options[1].value;
        const color = compo.options[2].value;
        const fontSize = compo.options[3].value;
        const titleParams = compo.options[0].params;
        const buttonAnchorVariableStyle = {
            backgroundColor: color,
            fontSize: parseInt(fontSize, 10),

        }

        return (

            <table className={"compo-component component-" + compo.name} border="0" width={'100%'}
                   style={Object.assign({}, defaultStyles.tableStyle, titleParams.titleStyles)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>
                <tr styles={{...defaultStyles.trStyle}}>
                    <th>
                        {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ''}
                        <a style={Object.assign({}, defaultStyles.buttonAnchorStyle, buttonAnchorVariableStyle)} href={link} target="_blank">
                            <b>{title}</b>
                        </a>

                    </th>
                </tr>
                </tbody>
            </table>
        )
    }
}

export default Button