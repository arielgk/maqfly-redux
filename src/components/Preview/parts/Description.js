import React from 'react';
import Edit from '../Edit';
import *  as defaultStyles from '../../DefaultStyles';
// add new components to Item.js



class Description extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })

    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })

    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;
        const title = compo.options[0].value;
        const titleParams = compo.options[0].params;

        const link = compo.options[1].value;
        const text = compo.options[2].value;
        const paragraphParams = compo.options[2].params;



        // const paragraphStyle = {
        //     ...paragraphParams.styles,
        //
        // }
        //
        // const titleStyles = {
        //     ...titleParams.styles,
        // }

        return (

            <table className={"compo-component component-" + compo.name} border="0" width={'100%'}
                   style={Object.assign({}, defaultStyles.tableStyle)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>
                <tr style={{...defaultStyles.trStyle}}>
                    <th style={{paddingRight: 30}}>
                        {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ""}

                        <div>
                            <a style={Object.assign({}, defaultStyles.anchorStyle, titleParams.styles)} href={link}
                               target="_blank">
                                {title}
                            </a>
                        </div>


                        <a style={Object.assign({}, paragraphParams.styles, defaultStyles.anchorStyle)} href={link} target="_blank"
                           dangerouslySetInnerHTML={this.createMarkup(text)}/>
                        <br/>
                        <br/>
                    </th>
                </tr>
                </tbody>

            </table>
        )
    }
}

export default Description