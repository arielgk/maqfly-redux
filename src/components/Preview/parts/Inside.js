import React from 'react';

const Inside = () => {
    return (

        <table className="body" style={{
            margin: 0,
            background: '#fafafa!important',
            borderCollapse: 'collapse',
            borderSpacing: 0,
            color: '#0a0a0a',
            fontFamily: 'Helvetica,Arial,sans-serif',
            fontSize: 14,
            fontWeight: 400,
            height: '100%',
            lineHeight: '1.3',
            padding: 0,
            textAlign: 'left',
            verticalAlign: 'top',
            width: '100%'
        }}>
            <tbody>
            <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                <td className="center" align="center" valign="top" style={{
                    MozHyphens: 'auto',
                    WebkitHyphens: 'auto',
                    margin: 0,
                    borderCollapse: 'collapse!important',
                    color: '#0a0a0a',
                    fontFamily: 'Helvetica,Arial,sans-serif',
                    fontSize: 14,
                    fontWeight: 400,
                    hyphens: 'auto',
                    lineHeight: '1.3',
                    padding: 0,
                    textAlign: 'left',
                    verticalAlign: 'top',
                    wordWrap: 'break-word'
                }}>
                    <center data-parsed style={{minWidth: 650, width: '100%'}}>
                        <table align="center" className="container float-center" style={{
                            margin: '0 auto',
                            background: '#fefefe',
                            borderCollapse: 'collapse',
                            borderSpacing: 0,
                            float: 'none',
                            padding: 0,
                            textAlign: 'center',
                            verticalAlign: 'top',
                            width: 650
                        }}>
                            <tbody>
                            <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                <td style={{
                                    MozHyphens: 'auto',
                                    WebkitHyphens: 'auto',
                                    margin: 0,
                                    borderCollapse: 'collapse!important',
                                    color: '#0a0a0a',
                                    fontFamily: 'Helvetica,Arial,sans-serif',
                                    fontSize: 14,
                                    fontWeight: 400,
                                    hyphens: 'auto',
                                    lineHeight: '1.3',
                                    padding: 0,
                                    textAlign: 'left',
                                    verticalAlign: 'top',
                                    wordWrap: 'break-word'
                                }}>
                                    <table className="row" style={{
                                        borderCollapse: 'collapse',
                                        borderSpacing: 0,
                                        display: 'table',
                                        padding: 0,
                                        position: 'relative',
                                        textAlign: 'left',
                                        verticalAlign: 'top',
                                        width: '100%'
                                    }}>
                                        <tbody>
                                        <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                            <th className="sidebar show-for-large small-4 large-3 columns first"
                                                style={{
                                                    margin: '0 auto',
                                                    color: '#0a0a0a',
                                                    fontFamily: 'Helvetica,Arial,sans-serif',
                                                    fontSize: 14,
                                                    fontWeight: 400,
                                                    lineHeight: '1.3',
                                                    padding: 0,
                                                    paddingBottom: 16,
                                                    paddingLeft: 16,
                                                    paddingRight: 8,
                                                    textAlign: 'left',
                                                    width: '65.25px'
                                                }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            {/*<table className="spacer" style={{*/}
                                                                {/*borderCollapse: 'collapse',*/}
                                                                {/*borderSpacing: 0,*/}
                                                                {/*padding: 0,*/}
                                                                {/*textAlign: 'left',*/}
                                                                {/*verticalAlign: 'top',*/}
                                                                {/*width: '100%'*/}
                                                            {/*}}>*/}
                                                                {/*<tbody>*/}
                                                                {/*<tr style={{*/}
                                                                    {/*padding: 0,*/}
                                                                    {/*textAlign: 'left',*/}
                                                                    {/*verticalAlign: 'top'*/}
                                                                {/*}}>*/}
                                                                    {/*<td height="16px" style={{*/}
                                                                        {/*MozHyphens: 'auto',*/}
                                                                        {/*WebkitHyphens: 'auto',*/}
                                                                        {/*margin: 0,*/}
                                                                        {/*borderCollapse: 'collapse!important',*/}
                                                                        {/*color: '#0a0a0a',*/}
                                                                        {/*fontFamily: 'Helvetica,Arial,sans-serif',*/}
                                                                        {/*fontSize: 16,*/}
                                                                        {/*fontWeight: 400,*/}
                                                                        {/*hyphens: 'auto',*/}
                                                                        {/*lineHeight: 16,*/}
                                                                        {/*msoLineHeightRule: 'exactly',*/}
                                                                        {/*padding: 0,*/}
                                                                        {/*textAlign: 'left',*/}
                                                                        {/*verticalAlign: 'top',*/}
                                                                        {/*wordWrap: 'break-word'*/}
                                                                    {/*}}>*/}
                                                                        {/*&nbsp;*/}
                                                                    {/*</td>*/}
                                                                {/*</tr>*/}
                                                                {/*</tbody>*/}
                                                            {/*</table>*/}
                                                            <img src="assets/img/untref.png" alt="b" style={{
                                                                 msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="right-side small-24 large-21 columns last" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: 0,
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: '0!important',
                                                textAlign: 'left',
                                                width: '552.75px'
                                            }}>
                                                content
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table className="row" style={{
                                        borderCollapse: 'collapse',
                                        borderSpacing: 0,
                                        display: 'table',
                                        padding: 0,
                                        position: 'relative',
                                        textAlign: 'left',
                                        verticalAlign: 'top',
                                        width: '100%'
                                    }}>
                                        <tbody>
                                        <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                            <th className="show-for-large no-padding small-12 large-4 columns first"
                                                style={{
                                                    margin: '0 auto',
                                                    color: '#0a0a0a',
                                                    fontFamily: 'Helvetica,Arial,sans-serif',
                                                    fontSize: 14,
                                                    fontWeight: 400,
                                                    lineHeight: '1.3',
                                                    padding: '0!important',
                                                    paddingBottom: 16,
                                                    paddingLeft: 16,
                                                    paddingRight: 8,
                                                    textAlign: 'left',
                                                    width: '92.33px'
                                                }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}/>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="show-for-large no-padding small-12 large-4 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: '0!important',
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'left',
                                                width: '92.33px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/redes_01.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="text-right no-padding small-4 large-2 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: '0!important',
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'right',
                                                width: '38.17px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/redes_02.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="text-right no-padding small-4 large-2 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: '0!important',
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'right',
                                                width: '38.17px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/redes_03.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="text-right no-padding small-4 large-2 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: '0!important',
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'right',
                                                width: '38.17px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/redes_04.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="text-right no-padding small-4 large-2 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: '0!important',
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'right',
                                                width: '38.17px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/redes_05.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="show-for-large small-12 large-6 columns" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: 0,
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 8,
                                                textAlign: 'left',
                                                width: '146.5px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}/>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                            <th className="logo-holder small-6 large-4 columns last" style={{
                                                margin: '0 auto',
                                                color: '#0a0a0a',
                                                fontFamily: 'Helvetica,Arial,sans-serif',
                                                fontSize: 14,
                                                fontWeight: 400,
                                                lineHeight: '1.3',
                                                padding: 0,
                                                paddingBottom: 16,
                                                paddingLeft: 8,
                                                paddingRight: 16,
                                                textAlign: 'left',
                                                width: '92.33px'
                                            }}>
                                                <table style={{
                                                    borderCollapse: 'collapse',
                                                    borderSpacing: 0,
                                                    padding: 0,
                                                    textAlign: 'left',
                                                    verticalAlign: 'top',
                                                    width: '100%'
                                                }}>
                                                    <tbody>
                                                    <tr style={{padding: 0, textAlign: 'left', verticalAlign: 'top'}}>
                                                        <th style={{
                                                            margin: 0,
                                                            color: '#0a0a0a',
                                                            fontFamily: 'Helvetica,Arial,sans-serif',
                                                            fontSize: 14,
                                                            fontWeight: 400,
                                                            lineHeight: '1.3',
                                                            padding: 0,
                                                            textAlign: 'left'
                                                        }}>
                                                            <img src="assets/img/untref_h.png" alt="b" style={{
                                                                msInterpolationMode: 'bicubic',
                                                                clear: 'both',
                                                                display: 'block',
                                                                maxWidth: '100%',
                                                                outline: 0,
                                                                textDecoration: 'none',
                                                                width: 'auto'
                                                            }}/>
                                                        </th>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </center>
                </td>
            </tr>
            </tbody>
        </table>
    )
}


export default Inside;