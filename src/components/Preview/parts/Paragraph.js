import React from 'react';
import Edit from '../Edit';
import *  as defaultStyles from '../../DefaultStyles';
// add new components to Item.js

class Paragraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })

    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })

    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;

        const link = compo.options[0].value;
        const text = compo.options[1].value;
        const paragraphParams = compo.options[1].params;



        return (

            <table className={"compo-component component-" + compo.name} border="0" width={'100%'}
                   style={Object.assign({}, defaultStyles.tableStyle)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>
                <tr style={{...defaultStyles.trStyle}}>
                    <th style={{paddingRight: 30}}>
                        {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ""}

                        {link === '#' || link === '' ?
                            <div>
                                <div style={Object.assign({}, paragraphParams.styles)}
                                     dangerouslySetInnerHTML={this.createMarkup(text)}/>
                            </div>

                            :

                            <div>
                                <a style={Object.assign({}, paragraphParams.styles, defaultStyles.anchorStyle)} href={link} target="_blank"
                                   dangerouslySetInnerHTML={this.createMarkup(text)}/>
                            </div>
                        }


                    </th>
                </tr>
                </tbody>

            </table>
        )
    }
}

export default Paragraph