import React from 'react';
import Edit from '../Edit';
import *  as defaultStyles from '../../DefaultStyles';

// add new components to Item.js

const titleStyles = {}


const componentStyle = {
    position: "relative",
    paddingRight: 16,
}

class MultiButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })
    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })
    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;
        const title = compo.options[0].value;
        const link = compo.options[1].value;

        const title_2 = compo.options[2].value;
        const link_2 = compo.options[3].value;
        const title_3 = compo.options[4].value;
        const link_3 = compo.options[5].value;

        const color = compo.options[6].value;
        const fontSize = compo.options[7].value;


        const titleParams = compo.options[0].params;
        const buttonAnchorVariableStyle = {
            backgroundColor: color,
            fontSize: parseInt(fontSize, 10),


        }

        const anchorStyle = {
            textDecoration: 'none',
            backgroundColor: color,
            padding: "10px",
            color: '#fff',
            margin: '0 10px 10px 0',
            display: 'inline-block',
            fontSize: parseInt(fontSize,10),


        }


        return (

            <table className={"compo-component component-" + compo.name}  width={'100%'} style={Object.assign({},componentStyle, titleParams.styles)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>


                {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ""}
                <tr style={{...defaultStyles.trStyle}}>
                    <th>
                        <a style={Object.assign({}, anchorStyle, titleStyles)} href={link} target="_blank">
                            <b>{title}</b>
                        </a>

                        <a style={Object.assign({}, anchorStyle, titleStyles)} href={link_2} target="_blank">
                            <b>{title_2}</b>
                        </a>


                        <a style={Object.assign({}, anchorStyle, titleStyles)} href={link_3} target="_blank">
                            <b>{title_3}</b>
                        </a>

                    </th>
                </tr>
                </tbody>


            </table>
        )
    }
}

export default MultiButton