import React from 'react';
import Edit from '../Edit';
import *  as defaultStyles from '../../DefaultStyles';
// add new components to Item.js


class TitleBajada extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })

    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })

    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;
        const title = compo.options[0].value;
        const titleParams = compo.options[0].params;
        const bajada = compo.options[1].value;
        const bajadaParams = compo.options[1].params;

        const link = compo.options[2].value;

        return (

            <table className={"compo-component component-" + compo.name} border="0" width={'100%'}
                   style={Object.assign({}, defaultStyles.tableStyle)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>
                <tr style={{...defaultStyles.trStyle}}>
                    <th style={{paddingRight: 30}}>
                        {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ""}
                        <div>
                            {link === '#' || link === '' ?

                                <table style={{...defaultStyles.tableStyle}}>
                                    <tr style={{ ...defaultStyles.trStyle}}>
                                        <th>
                                            <div style={Object.assign({},bajadaParams.styles)}>
                                                <br/>
                                                    {bajada}

                                            </div>
                                            <div style={Object.assign({}, titleParams.styles)}>
                                                    {title}
                                            </div>

                                        </th>
                                    </tr>
                                </table>

                                :
                                <a style={Object.assign({}, defaultStyles.anchorStyle, titleParams.styles)} href={link}
                                   target="_blank">
                                    {title}

                                </a>

                            }

                        </div>
                    </th>
                </tr>
                </tbody>

            </table>
        )
    }
}

export default TitleBajada