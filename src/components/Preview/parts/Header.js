import React from 'react';

import Edit from '../Edit';


// add new components to Item.js

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })

    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })

    }

    render() {
        const {compo, preview, index} = this.props;

        const link = compo.options[0].value;
        const imgurl = compo.options[1].value;


        const tableStyle = {
            borderCollapse: 'collapse',
            borderSpacing: '0px',
            display: 'table',
            padding: '0px',
            position:'relative',
            textAlign: 'left',
            verticalAlign: 'top',
            width: '100%',
        };

        const trStyle={
            padding: '0px',
            textAlign: 'left',
            verticalAlign: 'top',

        }




        return (

            <table className={"compo-component component-" + compo.name} border="0" width={'100%'}
                   style={Object.assign({}, tableStyle)}
                   onMouseEnter={this.mouseEnter}
                   onMouseLeave={this.mouseLeave}>
                <tbody>
                <tr styles={trStyle}>
                    <th>
                        {preview && this.state.hovered ? <Edit index={index} item={compo} /> : ""}
                        <a href={link} target="_blank">
                            <img src={imgurl}
                                 alt="b" style={{
                                msInterpolationMode: 'bicubic',
                                clear: 'both',
                                display: 'block',
                                maxWidth: '100%',
                                outline: 0,
                                textDecoration: 'none',
                                width: 'auto'
                            }}/>
                        </a>
                    </th>
                </tr>
                </tbody>



            </table>
        )
    }
}

export default Header