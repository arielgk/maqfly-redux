import React from 'react';
import Edit from '../Edit';


// add new components to Item.js


const anchorStyle = {
    textDecoration: 'none',
    color: "black",

}

const titleStyles = {}
const dateStyle = {
    fontSize: 12,
    color: "#333",
}


const componentStyle={
    position:"relative",
    paddingRight:16,
}
class Event extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hovered: false,
        }

        this.mouseLeave = this.mouseLeave.bind(this);
        this.mouseEnter = this.mouseEnter.bind(this);
    }

    mouseEnter(ev) {
        this.setState({
            hovered: true,
        })
    }

    mouseLeave(ev) {
        this.setState({
            hovered: false,
        })
    }

    createMarkup(html) {
        return {__html: html};
    };

    render() {
        const {compo, preview, index} = this.props;
        const title = compo.options[0].value;
        const link = compo.options[1].value;
        const date = compo.options[2].value;
        const text = compo.options[3].value;
        return (

            <div className={"compo-component component-" + compo.name} style={componentStyle}
                 onMouseEnter={this.mouseEnter}
                 onMouseLeave={this.mouseLeave}>
                {preview && this.state.hovered ? <Edit index={index} item={compo}/> : ""}

                <a style={Object.assign({}, anchorStyle, titleStyles)} href={link} target="_blank">
                    <b>{title}</b> <i style={dateStyle}>{date}</i><br/>
                </a>
                <a style={Object.assign({}, anchorStyle, titleStyles)} href={link} target="_blank"
                   dangerouslySetInnerHTML={this.createMarkup(text)}/>

            </div>
        )
    }
}

export default Event