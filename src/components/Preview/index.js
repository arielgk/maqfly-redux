import React from 'react'
// import { DragDropContext, Draggable, Droppable }  from 'react-beautiful-dnd'
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
class Preview extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sourceCode: '',
            showCode: false,
    }


    this.showCode = this.showCode.bind(this);
    }

    getHtml() {
        const RE_HTML_COMMENTS = /<!--[\s\S]*?-->/g;
        const html = this.fenceView.innerHTML.replace(RE_HTML_COMMENTS, '');


        this.setState({
            sourceCode: html,
        })
    }

    componentDidMount() {
        this.getHtml();


    }

    componentDidUpdate(nextProps) {
        if (this.props !== nextProps) {
            this.getHtml();
        }
    }
    showCode(){
        this.setState({
            showCode:!this.state.showCode,
        })
    }
    render() {


        const {items, activeItem, flyer, templates,settings} = this.props;
        const TagName = templates.filter((template) => {
            return template.name === flyer.template
        })[0].component;

        return (

            <div className="preview-container">

                <div className="show-code-button button" onClick={()=>{this.showCode();}}>Show Code</div>
                <Link className="show-preview-button button" to={"/preview/"+flyer.id} target="_blank">Show Preview</Link>
                <div className="preview preview-true">


                    <TagName preview={true} items={items} activeItem={activeItem} settings={settings}/>


                </div>

                <div className="source-container" style={{display: 'none'}} ref={(n) => (this.fenceView = n)}>
                    <TagName preview={true} items={items} activeItem={activeItem} settings={settings}/>
                </div>
                {this.state.showCode ?

                    <div className="codeModal">

                        <div className="codeContent">
                            <textarea name="sourcecode" value={this.state.sourceCode}/>
                        </div>
                        <div className="codeClose" onClick={()=>{this.showCode()}}>asd</div>
                    </div>

                    : ""}

            </div>

        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        items: state.items,
        activeItem: state.activeItem,
        flyer: ownProps.flyer,
        templates: state.templates,
        settings: state.settings,
    }
}
export default connect(mapStateToProps)(Preview);