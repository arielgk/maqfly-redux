import React from "react";


import Header from './parts/Header';
import Description from './parts/Description';
import Event from './parts/Event';
import Button from './parts/Button';
import MultiButton from './parts/MultiButton';
import Title from './parts/Title';
import Paragraph from "./parts/Paragraph";
import TitleBajada from "./parts/TitleBajada";

class Item extends React.Component{
    components = {
        Header: Header,
        Description: Description,
        Event:Event,
        Button:Button,
        MultiButton:MultiButton,
        Title:Title,
        Paragraph:Paragraph,
        TitleBajada :TitleBajada,
    };
    render(){
        const {item, preview, index}= this.props;

        const TagName = this.components[item.name];

        return(
            <TagName compo={item} preview={preview} index={index} />
        )
    }
}

export default Item;