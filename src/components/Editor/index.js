import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Compo from './Compo';
import Options from '../Options';
import Settings from '../Settings';
import {bindActionCreators} from "redux";
import * as itemsActions from "../../actions/itemsActions";
import * as flyerActions from "../../actions/flyerActions";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';

const editorStyle = {
    position: 'relative',
}

class Editor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            itemCount: 0,
            activeTab: this.props.activeTab,
        }

        this.sonCopy = this.sonCopy.bind(this);
        this.addItem = this.addItem.bind(this);
        this.onSelectTab = this.onSelectTab.bind(this);
    }

    sonCopy(src) {
        return JSON.parse(JSON.stringify(src));
    }

    addItem(compo) {

        const item = this.sonCopy(compo);
        item.flyer_id = this.props.flyer.id;
        item.order = this.state.itemCount + 1;
        this.props.actions.addItem(item)
    }

    componentDidUpdate(prevProps) {

        if (this.props.items !== prevProps.items) {
            this.setState({
                itemCount: this.props.items.length,

            })
        }
        if (this.props.activeTab !== prevProps.activeTab) {
            this.setState({
                activeTab: this.props.activeTab,
            })
        }

    }

    onSelectTab(index) {

        // this.setState({activeTab:tabIndex})}
        this.props.actions.setActiveTab(index);

    }

    componentDidMount() {

        this.props.actions.listItems(this.props.flyer.id);
    }

    render() {
        const {compos, items, activeItem, flyer, flyer:{settings}} = this.props;


        // TODO  Invalid prop `compo` of type `array` supplied to `Options`,  ----- line 107


        return (

            <div className="editor">

                <div style={editorStyle}>

                    <Tabs selectedIndex={this.state.activeTab} onSelect={this.onSelectTab}>
                        <TabList>
                            <Tab>Components</Tab>
                            <Tab>Edit</Tab>
                            <Tab>Settings</Tab>
                        </TabList>
                        <TabPanel>
                            <div className={'components-container'}>
                                {compos.map(c => <Compo key={c.name + '' + c.id} compo={c} addItem={this.addItem}/>)}
                            </div>


                        </TabPanel>

                        <TabPanel>

                            <div className="compo-editor">
                                {activeItem !== 9999 ? '' : 'Select an item to edit'}
                                {items.filter((i, index) => i.id === activeItem).map((i, index2) => {
                                    return <Options key={'compo-editor-' + index2} compo={i}
                                                    activeItem={activeItem}/>
                                })}
                            </div>
                        </TabPanel>
                        <TabPanel>

                            { settings && settings.length >0 ? <Settings compo={flyer}  activeItem={9999}/>: 'no settings' }
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        )
    }

}

Editor.propTypes = {
    compos: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired,
    activeItem: PropTypes.number.isRequired,
    activeTab: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired,
    flyer: PropTypes.object.isRequired,

}

const mapStateToProps = (state, ownProps) => {

    return {
        compos: state.compos.filter((c) => {
            return c.templates.indexOf(ownProps.flyer.template) !== -1

        }),
        items: state.items,
        activeItem: state.activeItem,
        activeTab: state.activeTab,
        flyer: ownProps.flyer,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions,...flyerActions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Editor)