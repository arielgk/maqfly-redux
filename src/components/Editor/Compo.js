import React from 'react';

// import Options from '../Options';
// import {bindActionCreators} from "redux";
// import * as actions from "../../actions/itemsActions";
// import {connect} from "react-redux";

// const Compo = ({compo, actions}) => {
//
//     const addItem = (compo)=>{
//         actions.addItem(compo);
//     }
//
//     return (
//
//         <div className={"compo " + compo.name}>
//             {compo.name}
//
//             <div className="button small" onClick={()=>{addItem(compo)}}> Add</div>
//
//             <div>
//                 {/*<Options compo={compo}/>*/}
//             </div>
//
//         </div>
//     )
// }

class Compo extends React.Component{
    render(){
        return(
            <div className={"compo compo-" + this.props.compo.name} onClick={()=>{this.props.addItem(this.props.compo)}}>
                {this.props.compo.name}


            </div>

        )
    }
}

// const mapDispatchToProps = (dispatch) => {
//
//     return {
//         actions: bindActionCreators({...actions}, dispatch)
//     };
// }
// export default connect(null, mapDispatchToProps)(Compo)
export default Compo


