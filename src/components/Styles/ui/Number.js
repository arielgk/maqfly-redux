import React from 'react';

// import PropTypes from 'prop-types';


class Number extends React.Component{
    constructor(props){
        super(props);

        this.state={
            value:props.value
        }
        this.onInputchange = this.onInputchange.bind(this);
    }

    onInputchange(ev){
        console.log('1');

        this.setState({
            value:ev.target.value,
        })
        this.props.onChange(this.props.property,parseInt(ev.target.value,10))
    }

    render(){
        const {property} = this.props;
        return(
            <div className={'number-editor'}>
                <div>
                    <label htmlFor={property}>{property}</label><br/>
                    <input type="number" name={property} onChange={this.onInputchange} value={this.state.value}/>

                </div>

            </div>
        )
    }
}

export default Number