import React from 'react';

// import PropTypes from 'prop-types';

import Number from '../ui/Number';


class Padding extends React.Component{


    render(){

        const {paddingTop, paddingRight,paddingBottom,paddingLeft} = this.props.values;
        const {onChange} = this.props;

        return(
            <div className={'padding'}>
                <div>
                    Padding:
                    <Number onChange={onChange} property={'paddingTop'} value={paddingTop} ></Number>
                    <Number onChange={onChange} property={'paddingRight'} value={paddingRight}></Number>
                    <Number onChange={onChange} property={'paddingBottom'} value={paddingBottom}></Number>
                    <Number onChange={onChange} property={'paddingLeft'} value={paddingLeft}></Number>

                </div>

            </div>
        )
    }
}




export default Padding;