import React from 'react';

// import PropTypes from 'prop-types';

import Number from '../ui/Number';


class Margin extends React.Component{


    render(){


        const {marginTop, marginRight,marginBottom,marginLeft} = this.props.values;
        const {onChange} = this.props;
        return(
            <div className={'margin'} style={{ display:'none'}}>
                <div>
                    Margin:

                    <Number onChange={onChange} property={'marginTop'} value={marginTop} ></Number>
                    <Number onChange={onChange} property={'maringRight'} value={marginRight}></Number>
                    <Number onChange={onChange} property={'marginBottom'} value={marginBottom}></Number>
                    <Number onChange={onChange} property={'marginLeft'} value={marginLeft}></Number>

                </div>

            </div>
        )
    }
}




export default Margin;