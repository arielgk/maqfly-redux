import React from 'react';

// import PropTypes from 'prop-types';

import Number from '../ui/Number';


class FontSize extends React.Component{


    render(){


        const {fontSize} = this.props.values;
        const {onChange} = this.props;
        return(
            <div className={'fontSize'}>
                <div>
                    FontSize:

                    <Number onChange={onChange} property={'fontSize'} value={fontSize} ></Number>

                </div>

            </div>
        )
    }
}




export default FontSize;