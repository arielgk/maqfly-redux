import React from 'react';

// import PropTypes from 'prop-types';

import Float from '../ui/Float';


class LineHeight extends React.Component{


    render(){


        const {lineHeight} = this.props.values;
        const {onChange} = this.props;
        console.log(this.props);
        return(
            <div className={'lineHeight'}>
                <div>
                    FontSize:

                    <Float onChange={onChange} property={'lineHeight'} value={lineHeight} ></Float>

                </div>

            </div>
        )
    }
}




export default LineHeight;