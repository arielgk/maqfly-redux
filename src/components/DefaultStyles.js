
export const defaultStyleParams = {

    marginTop: 0,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    fontSize:14,

};


export const titleDefaultStyleParams = {
    paddingTop: 20,
    paddingBottom: 20,
    fontSize:30,
    lineHeight: 1,
    display: 'block',
}

export const paragraphDefaultStyleParams = {
    fontWeight:200,
    fontSize:14,
    lineHeight: 1.3,
    paddingBottom: 20,

}


export const tableStyle = {
    borderCollapse: 'collapse',
    borderSpacing: '0px',
    display: 'table',
    padding: '0px',
    position: 'relative',
    textAlign: 'left',
    verticalAlign: 'top',
    width: '100%',
};

export const anchorStyle = {
    textDecoration: 'none',
    color: "black",

}

export const trStyle = {
    padding: '0px',
    textAlign: 'left',
    verticalAlign: 'top',

}

export const buttonAnchorStyle={
    textDecoration: 'none',
    padding: "10px",
    color: '#fff',
    margin: '0 0 10px 0',
    display: 'inline-block',

}

export const bajadaStyleParams = {
    fontSize:14,
    fontWeight:400,
    lineHeight:1,

}

export const titleBajadaStyleParams={
    paddingTop: 0,
    paddingBottom: 20,
    fontSize:30,
    lineHeight: 1,
    display: 'block',
}