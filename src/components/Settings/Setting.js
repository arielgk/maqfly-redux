import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as flyerActions  from '../../actions/flyerActions';

import TextField from '../Fields/TextField';
import TextArea from '../Fields/TextArea';
import FileUpload from '../Fields/FileUpload';
import NumericField from '../Fields/NumericField';



class Setting extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            option: props.option,
        }

        this.fields = {
            text: TextField,
            textarea: TextArea,
            fileupload: FileUpload,
            number: NumericField
        }

        this.onChange = this.onChange.bind(this);
        this.onChangeEditor = this.onChangeEditor.bind(this);
    }

    componentWillMount() {
        this.timer = null;
    }

    componentWillUnmount() {
        // console.log('unmount');

        if(this.timer){
            clearTimeout(this.timer);
        }

    }

    onChange = (e) => {
        e.persist()

        this.setState(prevState => (
                {
                    option: {...prevState.option, value: e.target.value}
                }
            ),
            () => {
                this.props.actions.changeSetting(this.state.option)
                // console.log('changed');
            }
        )

    }

    onChangeEditor =(value)=>{
        this.setState(prevState => (
                {
                    option: {...prevState.option, value}
                }
            ),
            () => {
                this.props.actions.changeSetting(this.state.option)

            }
        )
    }

    render() {

// console.log(this.state.option);
        const FieldType = this.fields[this.state.option.type];

        return (<div>
            <label htmlFor={this.state.option.name}>{this.state.option.label}</label>
            <FieldType option={this.state.option} optionindex={this.props.optionindex}  onChange={this.onChange}  onChangeEditor={this.onChangeEditor}/>

            <br/>
        </div>)
    }
}

Option.propTypes = {
    option: PropTypes.object.isRequired,

    actions: PropTypes.object.isRequired,

}

Option.defaultProps = {
    message: '',

};
const mapStateToProps = (state, ownProps) => {
    return {



    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...flyerActions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Setting)