import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as itemsActions from "../../actions/itemsActions";
import * as flyerACtions from "../../actions/flyerActions";


import Setting from './Setting';
import {bindActionCreators} from "redux";

/*
We are reusing this component for component option and flyer settings
should we?
    pros:
    we reusing all fields components

cons:
    it may be confusing for name convention and readability

    posible solution create Settings/index.js and move option fields outside Options Folder.

*/

class Settings extends React.Component {

    constructor(props) {
        super(props);

        this.saveSettings = this.saveSettings.bind(this);

    }



    saveSettings() {
        // maybe this conditions are unnecesary.
        //this code is unreachable is the activeItem is equal to 9999 and adn the active component is "Setting"
        // Although this seems dangerous
        const flyer={
            id:this.props.compo.id,
            settings:this.props.settings,

        }

        this.props.actions.saveSettings(flyer);

    }

    componentWillUnmount() {

        // should options be saved on unmount, leave it disabled for now.


        // this.props.actions.saveOptions(this.props.activeItem,this.props.opt);
    }

    render() {

        const {compo, activeItem, settings} = this.props;

        return (

            <div className="settings">

                <h4> {compo.name} {settings.length > 0 ? ' Settings' :' '}</h4>




                { settings.map((o, index) => <Setting

                        key={"settings-" + index}
                        optionindex={index}
                        saveOptions={this.saveSettings
                        } compo={compo}
                        option={o}
                    />)
                }


                <div className='save-button' onClick={() => {

                        this.saveSettings();

                }}>
                    Save
                </div>
            </div>
        )
    }
}

Settings.propTypes = {
    // compos: PropTypes.array.isRequired,
    compo: PropTypes.object.isRequired,
    activeItem: PropTypes.number.isRequired,

    actions: PropTypes.object.isRequired,
    settings:PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {

    return {
        // compos: ownProps.compos,
        compo: ownProps.compo,
        activeItem: state.activeItem ? state.activeItem : 9999,
        settings:state.settings,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions,...flyerACtions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Settings);