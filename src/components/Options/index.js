import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as itemsActions from "../../actions/itemsActions";
import * as flyerACtions from "../../actions/flyerActions";

import Option from './Option';

import {bindActionCreators} from "redux";



class Options extends React.Component {

    constructor(props) {
        super(props);

        this.saveOptions = this.saveOptions.bind(this);


    }

    saveOptions(activeItem) {


            this.props.actions.saveOptions(activeItem);
    }





    componentWillUnmount() {

        // should options be saved on unmount, leave it disabled for now.


        // this.props.actions.saveOptions(this.props.activeItem,this.props.opt);
    }

    render() {

        const {compo, activeItem, settings} = this.props;

        return (

            <div className="options">

                <h4> {compo.name} </h4>




                { compo.options.map((o, index) => <Option
                        key={activeItem + '-' + compo.name + "-" + index}
                        optionindex={index}
                        saveOptions={this.saveOptions
                        } compo={compo}
                        option={o}/>)

                }


                <div className='save-button' onClick={() => {
                        this.saveOptions(this.props.activeItem);


                }}>
                    Save
                </div>
            </div>
        )
    }
}

Options.propTypes = {
    // compos: PropTypes.array.isRequired,
    compo: PropTypes.object.isRequired,
    activeItem: PropTypes.number.isRequired,

    actions: PropTypes.object.isRequired,
    settings:PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {

    return {
        // compos: ownProps.compos,
        compo: ownProps.compo,
        activeItem: state.activeItem ? state.activeItem : 9999,
        settings:state.settings,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions,...flyerACtions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Options);