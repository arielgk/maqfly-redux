import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as itemsActions from '../../actions/itemsActions';

import TextField from '../Fields/TextField';
import TextArea from '../Fields/TextArea';
import FileUpload from '../Fields/FileUpload';
import NumericField from '../Fields/NumericField';
import ColorField from '../Fields/ColorField';

// const TextField = ({option,onChange})=>{
//     return(
//         <input type={option.type} name={option.key} value={option.value}
//                onChange={onChange}/>
//     )
// }
//
// const TextArea = ({option,onChange})=>{
//     return(
//         <textarea type={option.type} rows="20" name={option.key} value={option.value}
//                onChange={onChange}/>
//     )
// }

// const WAIT_INTERVAL = 1000;
// const ENTER_KEY = 13;

class Option extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            option: props.option,
        }

        this.fields = {
            text: TextField,
            textarea: TextArea,
            fileupload: FileUpload,
            number: NumericField,
            color:ColorField,
        }

        this.onChange = this.onChange.bind(this);
        this.onChangeEditor = this.onChangeEditor.bind(this);
    }

    componentWillMount() {
        this.timer = null;
    }

    componentWillUnmount() {
        // console.log('unmount');
        this.props.saveOptions(this.props.activeItem);
    }

    onChange = (e) => {
        e.persist()

        this.setState(prevState => (
                {
                    option: {...prevState.option, value: e.target.value}
                }
            ),
            () => {
                this.props.actions.changeOption(this.props.activeItem, this.state.option, false)
                // console.log('changed');
            }
        )

    }

    onChangeEditor =(value)=>{
        this.setState(prevState => (
                {
                    option: {...prevState.option, value}
                }
            ),
            () => {
                this.props.actions.changeOption(this.props.activeItem, this.state.option, false)
                // console.log('changed');
            }
        )
    }

    render() {

// console.log(this.state.option);
        const FieldType = this.fields[this.state.option.type];

        return (<div>
            <label htmlFor={this.state.option.name}>{this.state.option.label}</label>
            <FieldType option={this.state.option} optionindex={this.props.optionindex}  onChange={this.onChange}  onChangeEditor={this.onChangeEditor}/>


        </div>)
    }
}

Option.propTypes = {
    option: PropTypes.object.isRequired,
    activeItem: PropTypes.number.isRequired,
    actions: PropTypes.object.isRequired,

}

Option.defaultProps = {
    message: '',

};
const mapStateToProps = (state, ownProps) => {
    return {
        activeItem: state.activeItem,


    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...itemsActions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Option)