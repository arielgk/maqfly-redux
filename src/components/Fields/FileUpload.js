import React, { Component } from 'react';
import superagent from 'superagent';
import * as config from '../../config';

class Modal extends Component {
    constructor(props) {
        super(props);


        this.state = {
            submit: false,
            position: this.props.position,
            apiUrl:config.BASE_URL,
            preload: false,
            params: this.props.params,


        }
        this.onFileSelected = this.onFileSelected.bind(this)
        this.onPositionChange = this.onPositionChange.bind(this)
        this.onFileUploadHandler = this.onFileUploadHandler.bind(this);
    }

    onPositionChange(ev) {

    }

    onFileSelected(ev) {
        this.setState({
            submit: true,
        })
    }

    onFileUploadHandler() {
        this.setState({
            preload: true,
        })

        let files = document.getElementById('file_to_upload').files;
        let self = this;
        superagent.post(this.state.apiUrl+'/upload')
            .set('accept', 'json')
            .attach('file', files[0])
            .field('position', this.state.position)
            .field('params', JSON.stringify(this.state.params))
            .end(function (err, res) {
                console.log(res.text);
                self.setState({
                    preload: false,
                })
                 self.props.onImageAdded(self.state.apiUrl + '/images/' + res.text.slice(1, -1));
               // self.props.onImageAdded("https://html5box.com/html5lightbox/images/skynight.jpg",1);

            });

    }

    render() {

        return (
            <div className="react-modal-container">
                <div className="react-modal-popup">
                    <div className="react-modal-header">
                        Subir Imagen: {this.state.position}
                        <hr />
                    </div>
                    <div className="react-modal-content">
                        <form id="image-upload" encType="multipart/form-data">
                            <input type="hidden" name="position" defaultValue={this.state.position}
                                   onChange={this.onPositionChange} />
                            <input type="file" id="file_to_upload" name="file_to_upload"
                                   onChange={this.onFileSelected} />
                        </form>
                        {this.state.preload ?
                            <div className="react-modal-preload"><img src="http://1203media.com/images/preload.gif"
                                                                      alt="" /></div> : ''}


                        <br /><br /><br />
                    </div>
                    <div className="react-modal-footer">
                        <hr />
                        <div className="react-modal-button react-button" onClick={this.props.toggleModal}>Cerrar</div>
                        {this.state.submit ?
                            <div id="react-modal-submit" className="react-button" onClick={this.onFileUploadHandler}>
                                Subir Imagen</div> : ""}
                    </div>
                </div>
            </div>
        )
    }
}

class Launcher extends Component {
    render() {
        return (<div className="react-button" onClick={this.props.toggleModal}>Subir </div>)
    }
}


class ImagePreview extends Component {
    render() {
        const previewStyle = {
            maxWidth: this.props.maxWidth + 'px',
        }
        return (
            <div style={previewStyle} className="react-image-upload-preview">
                <div className="react-image-upload-preview-toolbar">
                    <span className="close" onClick={this.props.deleteImage}><i>x</i></span>
                </div>
                <div className="react-image-upload-preview-image">

                    <img src={this.props.imageUrl} alt="" />
                </div>
            </div>

        );
    }
}


class ImageUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            launcher: true,
            position: this.props.option.key,
            params:this.props.option.params,
            imageId: (this.props.imageId && this.props.imageId !== '' ? this.props.imageId : null),
            imageUrl: (this.props.option.value && this.props.option.value !== '' ? this.props.option.value : null),
            maxWidth: (this.props.maxWidth && this.props.maxWidth !== '' ? this.props.maxWidth : 220),

        }
        this.inputRef = React.createRef();


        this.toggleModal = this.toggleModal.bind(this);
        this.onImageUrlChangeHandler = this.onImageUrlChangeHandler.bind(this);
        this.onImageAdded = this.onImageAdded.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
    }

    toggleModal() {
        this.setState({
            modal: !this.state.modal
        })
    }

    onImageAdded(imageUrl) {
        this.setState({

            imageUrl: imageUrl,
            modal: !this.state.modal
        },()=>{
            let input = this.inputRef.current;
            this.setNativeValue(input, 'some text');
            input.dispatchEvent(new Event('input', { bubbles: true }));

        })
    }

    onImageUrlChangeHandler(ev) {
        console.log('added');
    }

    setNativeValue(element, value) {
        const valueSetter = Object.getOwnPropertyDescriptor(element, 'value').set;
        const prototype = Object.getPrototypeOf(element);
        const prototypeValueSetter = Object.getOwnPropertyDescriptor(prototype, 'value').set;

        if (valueSetter && valueSetter !== prototypeValueSetter) {
            prototypeValueSetter.call(element, value);
        } else {
            valueSetter.call(element, value);
        }
    }

    deleteImage() {
        console.log('delete');
        this.setState({
            imageUrl: '',
        },()=>{

            let input = this.inputRef.current;
            this.setNativeValue(input, 'some text');
            input.dispatchEvent(new Event('input', { bubbles: true }));

            //
            // input.trigger('input')
            // console.log(input);
            // this.props.onChange()
        })
    }

    render() {


        return (

            <div className="ImageUpload">


                <input  ref={this.inputRef} id={this.state.position+'-input'} type="text" name={this.state.position} value={this.state.imageUrl}
                       onChange={this.props.onChange} />
                {this.state.imageUrl !== '' && this.state.imageUrl ?
                    <ImagePreview maxWidth={this.state.maxWidth} deleteImage={this.deleteImage}
                                  imageUrl={this.state.imageUrl} /> : ''}
                {this.state.imageUrl === '' || this.state.imageUrl === null ?
                    <Launcher toggleModal={this.toggleModal} position={this.state.position} /> : ''}
                {this.state.modal ?
                    <Modal onImageAdded={this.onImageAdded} opt={this.props.opt} position={this.state.position} params={this.state.params}
                           toggleModal={this.toggleModal} /> : ""}
                <br />
            </div>
        );
    }
}


export default ImageUpload
