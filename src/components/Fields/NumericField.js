import React from 'react';

const NumericField = ({option,onChange})=>{
    return(
        <input type={option.type} name={option.key} value={option.value}
               onChange={onChange}/>
    )
}


export default NumericField;
