import React from 'react';
import Styles from '../index'
import {IoIosOptions }  from 'react-icons/io';
import { GithubPicker } from 'react-color';

const WAIT_INTERVAL = 1000;
// const ENTER_KEY = 13;

class TextField extends React.Component {

    constructor(props) {
        super(props);

        this.onFieldChange = this.onFieldChange.bind(this);
        this.state = {
            textFieldValue: props.option.value,
            showParams:false,
        }

        this.showParams = this.showParams.bind(this);
    }

    componentWillMount() {
        this.timeOut = null;
    }

    onFieldChange(ev) {
        ev.persist();
        if (this.timeOut) {
            clearTimeout(this.timeOut);
        }
        this.setState({
            textFieldValue:ev.target.value,
        })

        this.timeOut = setTimeout(() => {
            this.triggerChange(ev.target.value);
        }, WAIT_INTERVAL);
    }

    triggerChange(value) {

        this.props.onChangeEditor(value)
    }

    showParams(){

        this.setState({
            showParams:!this.state.showParams,
        });

    }
    render() {
        const {option, optionindex} = this.props


        return (
            <div>
                <div className={option.params.styles ? 'input-with-options' : ''}>
                    <input className={''} type={option.type} name={option.key} value={this.state.textFieldValue}
                           onChange={this.onFieldChange}/>



                    {option.params.styles ?
                        <IoIosOptions onClick={() => {
                            this.showParams();
                        }}/>
                    :''}
                </div>

                {option.params.styles && Object.keys(option.params.styles ).length !== 0 && this.state.showParams ?

                    <Styles option={option} optionindex={optionindex} styles={option.params.styles}/>
                    :''}

            </div>


        )
    }
}

export default TextField;
