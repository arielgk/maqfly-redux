import React from 'react';
import {
    EditorState,
    convertFromHTML,
    convertToRaw,
    ContentState,
    // createFromText,
    Modifier,
} from 'draft-js';

import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
// import {convertToHTML} from 'draft-convert';
import createCodeEditorPlugin from 'draft-js-code-editor-plugin';
import {stateFromHTML} from 'draft-js-import-html';

import {Editor} from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
// import Immutable from 'immutable';
import {IoIosOptions }  from 'react-icons/io';
import Styles from '../index'


const WAIT_INTERVAL = 1000;
const ENTER_KEY = 13;

// const blockRenderMap = Immutable.Map({
//     'header-two': {
//         element: 'h2'
//     },
//     'unstyled': {
//         element: 'h2'
//     }
// });


const styleMap = {
    'STRIKETHROUGH': {
        color: 'red',
    },
};
class TextArea extends React.Component {
    constructor(props) {
        super(props);

        let editorState;

        if (props.option.value) {
            const blocksFromHTML = convertFromHTML(props.option.value);
            const contentState = ContentState.createFromBlockArray(blocksFromHTML);
            editorState = EditorState.createWithContent(contentState);
        }
        else {
            editorState = EditorState.createEmpty();
        }

        this.state = {
            debugHtml: false,
            editorState,
            plugins: [createCodeEditorPlugin()],
            showParams:false,
        };
        this.showParams = this.showParams.bind(this);

        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.toggleHtml = this.toggleHtml.bind(this);
        this.handlePastedText = this.handlePastedText.bind(this);

    }

    handlePastedText(text, html){
        const { editorState } = this.state
        const blockMap = stateFromHTML(html).blockMap
        const newState = Modifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), blockMap)
        this.onEditorStateChange(EditorState.push(editorState, newState, 'insert-fragment'))
        return true
    }


    componentWillMount() {
        this.timeOut = null;
    }

    toggleHtml() {
        this.setState({
            debugHtml: !this.state.debugHtml,
        })
    }

    triggerChange() {


        this.props.onChangeEditor(draftToHtml(convertToRaw(this.state.editorState.getCurrentContent())))
    }

    handleKeyDown(e) {
        if (e.keyCode === ENTER_KEY) {
            this.triggerChange();
        }
    }

    onEditorStateChange(editorState) {

        if (this.timeOut) {
            clearTimeout(this.timeOut);
        }

        this.setState({
            editorState,
        });
        this.timeOut = setTimeout(() => {
            this.triggerChange();
        }, WAIT_INTERVAL);

    }

    componentWillUnmount() {
        if (this.timeOut) {
            clearTimeout(this.timeOut);
        }
    }

    setEditorReference = (ref) => {
        this.editorReference = ref;

    }

    showParams(){

        this.setState({
            showParams:!this.state.showParams,
        });

    }

    render() {
        const {option, onChange,optionindex} = this.props;


        return (
            <div>
                <div style={{color: '#333', background: '#fff'}} className={option.params.styles ? 'input-with-options' : ''}>
                    <Editor
                        className={'editor-textarea'}
                        handlePastedText={this.handlePastedText}


                        editorState={this.state.editorState}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                        // blockRenderMap={blockRenderMap}
                        customStyleMap={styleMap}

                        plugins={this.state.plugins}
                        onKeyDown={this.handleKeyDown}

                        onEditorStateChange={this.onEditorStateChange}
                    />

                    {option.params.styles ?
                        <IoIosOptions onClick={() => {
                            this.showParams();
                        }}/>
                        :''}

                </div>
                <div style={{fontSize: "9px", margin: '10px 0', cursor: 'pointer',textAlign:'right'}} onClick={this.toggleHtml}> HTML
                </div>

                {option.params.styles && Object.keys(option.params.styles ).length !== 0 && this.state.showParams ?

                    <Styles option={option} optionindex={optionindex} styles={option.params.styles}/>
                    :''}

                {this.state.debugHtml ?
                    <textarea type={option.type} rows="20" name={option.key}
                              value={draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))}
                              onChange={onChange}/>
                    : ''}
            </div>
        )
    }
}

export default TextArea