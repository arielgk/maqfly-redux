import React from 'react';
import PropTypes from 'prop-types';
import Editor from '../Editor'
import Preview from '../Preview'
import {connect} from 'react-redux';
import * as actions from "../../actions/flyerActions";
import {Link} from 'react-router-dom'
import {bindActionCreators} from "redux";

class Flyer extends React.Component {

    componentDidMount() {
        this.props.actions.getFlyer(this.props.match.params.id, this.props.opt)
    }

    componentDidUpdate(nextProps) {
        // if(this.props !== nextProps){
        //     this.props.actions.getFlyer(this.props.match.params.id, this.props.opt)
        // }

    }

    render() {

        const {flyer } = this.props;
        return (
            <div className={''}>
                <ul className={'navigation'}>
                    <li><Link to="/flyers">Flyers</Link></li>
                </ul>


                {flyer ?
                    <div className="container">


                        <div>
                            <Preview flyer={flyer}/>
                            <Editor flyer={flyer} />

                        </div>


                    </div>
                    : <div>none</div>}
            </div>
        )

    }
}

Flyer.propTypes = {
    actions: PropTypes.object.isRequired,
    opt: PropTypes.object.isRequired,
    flyers: PropTypes.array.isRequired,
}

const mapStateToProps = (state, ownProps) => {

    let flyer = state.flyers.filter((i, index) => {
        return i.id === parseInt(ownProps.match.params.id, 10);
    })

    return {
        opt: state.maqfly,
        flyer: flyer.length > 0 ? flyer[0] : null,
        flyers: state.flyers,

    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Flyer)