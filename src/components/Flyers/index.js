import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'

import {bindActionCreators} from "redux";
import * as actions from "../../actions/flyerActions";

class Flyers extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            template: 'novedades',
        }

        this.changeName = this.changeName.bind(this);
        this.changeTemplate = this.changeTemplate.bind(this);
        this.createFlyer = this.createFlyer.bind(this);

    }

    createFlyer() {
        console.log('create-flyer');

        const template = this.props.templates.filter( (t,index)=>{
            return t.name === this.state.template;
        })
        const flyer = {
            name: this.state.name,
            template: this.state.template,
            settings:template.length > 0 ? template[0].settings : null ,

        }

        this.props.actions.createFlyer(flyer)
    }

    changeName(ev) {

        this.setState({
            name: ev.target.value,
        })
    }

    changeTemplate(ev) {
        this.setState({
            template: ev.target.value,
        })
    }

    componentDidMount() {

        this.props.actions.getFlyers();

    }

    render() {
        const {flyers , templates,actions} = this.props;

        return (
            <div className="flyers">

                <div className="create-flyer-container">
                    <div className="field-group">
                        <label htmlFor="nombre">Name</label><br/>
                        <input type="text" name='nombre' onChange={this.changeName}/>
                    </div>
                    <div className="field-group">
                        <label htmlFor="template">Template</label><br/>
                        <select name="template" id="template" onChange={this.changeTemplate}>

                            {/*<option value="">Select</option>*/}
                            {templates.map((template, index) => {
                                return <option key={'template-' + index} value={template.name}>{template.name}</option>
                            })
                            }


                        </select>
                    </div>
                    <br/>

                    {this.state.name.length > 0 ?
                        <div className="create-flyer-button">
                            <div className="button" onClick={this.createFlyer}>Create Flyer</div>
                        </div>
                        : ''}
                    <br/>
                    <br/>

                </div>

                <div className="flyers-table">


                {flyers.map((i, index) => {
                    return <div key={'flyer-' + i.id + '-' + index} className={'flyers-row'}>
                        <div className={'flyers-title'}>
                            {i.name}
                        </div>
                        <div className={'flyers-tools'}>
                           <div onClick={()=>{
                               actions.duplicateFlyer(i.id)
                           }}>Duplicate</div>
                            <Link to={'/flyers/' + i.id} flyer={i}>Edit</Link>
                        </div>


                    </div>
                })}
                </div>

            </div>
        )
    }
}

Flyers.propTypes = {
    flyers: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,

    templates: PropTypes.array.isRequired,

}

const mapStateToProps = (state, ownProps) => {

    return {
        flyers: state.flyers,
        opt: state.maqfly,
        templates: state.templates,
    }
}
const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Flyers);