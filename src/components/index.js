import React from 'react';
import PropTypes from 'prop-types';

import Margin from './Styles/properties/Margin';
import Padding from './Styles/properties/Padding';
import FontSize from './Styles/properties/FontSize';
import LineHeight from "./Styles/properties/LineHeight";

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from "../actions/stylesActions";


class Styles extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        this.props.actions.setStyles(this.props.styles)
    }

    onChange(property,value){

        this.props.actions.changeStyleProperty(this.props.activeItem,this.props.option.key, property,value, this.props.optionindex);
    }

    render() {

        const {option} = this.props;
        return (
            <div className={'params'}>

                <FontSize onChange={this.onChange} values={option.params.styles}></FontSize>
                <LineHeight onChange={this.onChange} values={option.params.styles}></LineHeight>
                <Margin onChange={this.onChange} values={option.params.styles}></Margin>
                <Padding onChange={this.onChange} values={option.params.styles}></Padding>
            </div>
        )
    }
}

Styles.propTypes = {

    option: PropTypes.object.isRequired,
    styles: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    activeItem: PropTypes.number.isRequired,
}

const mapStateToProps = (state, ownProps) => {

    return {
        option:ownProps.option,
        styleProperties: state.styles,
        activeItem:state.activeItem,
    }
}

const mapDispatchToProps = (dispatch) => {

    return {
        actions: bindActionCreators({...actions}, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Styles);