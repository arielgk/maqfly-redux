import React, {Component} from 'react';
import Flyers from './Flyers';
import Flyer from './Flyers/Flyer';
import RemoteFlyer from './Preview/remote/RemoteFlyer';
// import Container from './Container';


import {BrowserRouter as Router, Route   , Switch} from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">

                    <Switch>
                        <Route path="/" exact={true} component={Flyers}/>
                        <Route path="/flyers"  exact={true} component={Flyers}/>
                        <Route path="/flyers/:id"  exact={true} component={Flyer}/>
                        <Route path="/preview/:id"  exact={true} component={RemoteFlyer}/>
                    </Switch>

                </div>
            </Router>
        );
    }
}

export default App;
