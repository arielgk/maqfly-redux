import Novedades from "../components/Preview/templates/Novedades";
import InfoUntref from "../components/Preview/templates/InfoUntref";
import MaqFly from "../components/Preview/templates/Maqfly";
import * as defaultStyles from '../components/DefaultStyles';
// const defaultStyleParams = {
//
//     marginTop: 0,
//     marginRight: 0,
//     marginBottom: 0,
//     marginLeft: 0,
//     paddingTop: 0,
//     paddingRight: 0,
//     paddingBottom: 0,
//     paddingLeft: 0,
//     fontSize:14,
//
// };
//
//
// const titleDefaultStyleParams = {
//     paddingTop: 20,
//     paddingBottom: 20,
//     fontSize:30,
//     lineHeight: 1,
//     display: 'block',
// }
//
// const paragraphDefaultStyleParams = {
//     fontWeight:200,
//     fontSize:14,
//     lineHeight: 1.3,
//     paddingBottom: 20,
//
// }




export default {
    maqfly: {
        post_url: '//localhost:8000'
    },
    compos: [
        {
            name: 'Header',
            desc: 'desc',
            options: [
                {
                    label: 'Link',
                    key: 'link',
                    type: 'text',
                    params: {},
                    value: 'https://',

                },
                {
                    label: 'Imagen URL upload',
                    key: 'upload',
                    type: 'fileupload',
                    params: {
                        height: 600,
                        width: 422,
                        crop: false,
                        styles: defaultStyles.defaultStyleParams,

                    },
                    value:
                        'http://untref.edu.ar/mailing_untref/infountref2018/168/images/img_1.png?v=5b25c1c702185',

                }
            ],
            templates: ['novedades'],

        },
        {
            name: 'Title',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.titleDefaultStyleParams),
                        },
                        value: 'Aenean commodo ligula eget dolor',
                    },

                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params: {},
                        value: '#',
                    },

                ],
            templates: ['novedades'],

        },
        {
            name: 'TitleBajada',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.titleBajadaStyleParams),
                        },
                        value: 'Aenean commodo ligula eget dolor',
                    },
                    {
                        label: 'Subtitle',
                        key: 'bajada',
                        type: 'text',
                        params: {
                            styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.bajadaStyleParams),
                        },
                        value: 'Aenean commodo ligula eget dolor',
                    },

                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params: {},
                        value: '#',
                    },

                ],
            templates: ['novedades'],

        },
        {
            name: 'Paragraph',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params:{},
                        value: '#',
                    }
                    , {
                    label: 'Text',
                    key: 'text',
                    type: 'textarea',
                    params: {
                        styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.paragraphDefaultStyleParams),
                    },
                    value: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>'
                },

                ],
            templates: ['novedades'],

        }
        ,
        {
            name: 'Description',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.titleDefaultStyleParams),
                        },
                        value: 'Aenean commodo ligula eget dolor',
                    },

                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                       params:{},
                        value: '#',
                    }
                    , {
                    label: 'Text',
                    key: 'text',
                    type: 'textarea',
                    params: {
                        styles:Object.assign({}, defaultStyles.defaultStyleParams,defaultStyles.paragraphDefaultStyleParams),
                    },
                    value: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>'
                },

                ],
            templates: ['novedades'],

        }
        ,
        {
            name: 'Event',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles: defaultStyles.defaultStyleParams,
                        },
                        value: 'Lorem ipsum dolor sit amet',
                    },
                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params: {},
                        value: '#',
                    },
                    {
                        label: 'Date',
                        key: 'date',
                        type: 'text',
                        params: {},
                        value: '30-05-2020 13:00hs',
                    },
                    {
                        label: 'Text',
                        key: 'text',
                        type: 'textarea',
                        params: {
                            styles: defaultStyles.paragraphDefaultStyleParams,
                        },
                        value: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa</p>',
                    },

                ],
            templates: ['novedades'],

        },

        {
            name: 'Button',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles: defaultStyles.defaultStyleParams,
                        },
                        value: 'Lorem ipsum dolor sit amet',
                    },
                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params: {},
                        value: '#',
                    },
                    {
                        label: 'Color',
                        key: 'color',
                        type: 'color',
                        params: {},
                        value: '#1a7bc1',
                    },
                    {
                        label: 'FontSize',
                        key: 'fontsize',
                        type: 'number',
                        params: {},
                        value: '12',
                    },

                ],
            templates: ['novedades'],

        },
        {
            name: 'MultiButton',
            desc:
                'desc',
            options:
                [
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                            styles: defaultStyles.defaultStyleParams,
                        },
                        value: 'Lorem ipsum ',
                    },
                    {
                        label: 'Link',
                        key: 'link',
                        type: 'text',
                        params: {},
                        value: '#',
                    },
                    {
                        label: 'Title 2',
                        key: 'title_2',
                        type: 'text',
                        params: {
                            styles: defaultStyles.defaultStyleParams,
                        },
                        value: 'Lorem ipsum ',
                    },
                    {
                        label: 'Link 2',
                        key: 'link_2',
                        type: 'text',
                        params: {},
                        value: '#',
                    },
                    {
                        label: 'Title 3',
                        key: 'title_3',
                        type: 'text',
                        params: {
                            styles: defaultStyles.defaultStyleParams,
                        },
                        value: 'Lorem ipsum ',
                    },
                    {
                        label: 'Link 3',
                        key: 'link_3',
                        type: 'text',
                        params: {},
                        value: '#',
                    },
                    {
                        label: 'Color',
                        key: 'color',
                        type: 'text',
                        params: {},
                        value: '#1a7bc1',
                    },
                    {
                        label: 'FontSize',
                        key: 'fontsize',
                        type: 'number',
                        params: {},
                        value: '12',
                    },

                ],
            templates: ['maqfly','novedades'],

        },
    ],
    items: [
        // {
        //     id:1,
        //     name:'Description',
        //     desc:'desc',
        //     options:[
        //         {
        //             label:'titulo',
        //             key:'title',
        //             type:'text',
        //             value:'sample',
        //         },
        //         {
        //             label:'BLA',
        //             key:'bla',
        //             type:'text',
        //             value:'sample',
        //         }
        //     ]
        //
        // },

    ],
    flyers:
        [
            // {
            //     id: 1,
            //     name: 'bla',
            //     template: 'novedades'
            // },
            // {
            //     id: 2,
            //     name: 'bla2',
            //     template: 'infountref'
            // }
        ],
    activeItem:
        9999,
    ajaxCallInProgress:
        0,
    //al agregar un nuevo template , agregar tambien import a /src/components/Preview/index.js
    templates:
        [
            {
                name: 'novedades',
                component: Novedades,
                settings:[
                    {
                        label: 'Title',
                        key: 'title',
                        type: 'text',
                        params: {
                        },
                        value: 'Lorem ipsum ',
                    },
                    {
                        label: 'Title 2',
                        key: 'title-2',
                        type: 'text',
                        params: {
                        },
                        value: 'Lorem ipsum 2',
                    },
                    {
                        label: 'Imagen URL upload',
                        key: 'upload',
                        type: 'fileupload',
                        params: {
                            height: 600,
                            width: 422,
                            crop: false,
                            styles: defaultStyles.defaultStyleParams,

                        },
                        value:
                            'http://untref.edu.ar/mailing_untref/infountref2018/168/images/img_1.png?v=5b25c1c702185',

                    }
                ],
            },
            {
                name: 'infountref',
                component: InfoUntref
            },
            {
                name: 'maqfly',
                component: MaqFly
            },

        ],
    activeTab: 0,
    styles: {},

    settings:[],
}
