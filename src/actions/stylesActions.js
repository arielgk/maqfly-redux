import * as types from "./actionTypes";
// import * as config from "../config";

export const setStyles = (styles) => {
    console.log(styles);
    return (dispatch) => {

        dispatch({type: types.SET_STYLES, styles});

    }

}

export const changeStyleProperty = (activeItem,key,property, value, index) => {
    return (dispatch) => {
        dispatch({type: types.CHANGE_STYLE_PROPERTY, payload: {activeItem:activeItem,key:key,property: property, value: value, index:index}});
    }
}