import * as types from "./actionTypes";
import * as config from "../config";
import axios from 'axios';
import {beginAjaxCall, ajaxCallError} from "./ajaxStatusActions";

export const createFlyer = (flyer) => {
    console.log(flyer);
    return (dispatch) => {
        dispatch(beginAjaxCall());

        axios.post(config.BASE_URL + '/flyer', {name: flyer.name, template: flyer.template, settings: flyer.settings})
            .then(function (response) {

                dispatch({type: types.ADD_FLYER_SUCCESS, flyer: response.data});

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }
}

export const getFlyers = () => {
    return (dispatch) => {
        dispatch(beginAjaxCall());

        axios.get(config.BASE_URL + '/flyer')
            .then(function (response) {

                dispatch({type: types.GET_FLYERS_SUCCESS, flyers: response.data});

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }
}

export const getFlyer = (flyerId) => {

    return (dispatch) => {
        dispatch(beginAjaxCall());

        axios.get(config.BASE_URL + '/flyer/' + flyerId)
            .then(function (response) {

                dispatch({type: types.GET_FLYER_SUCCESS, flyer: response.data});

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }
}

export const publishFlyer = () => {

    return (dispatch) => {


    }

}

export const saveSettings= (flyer) => {
    // TODO
    return dispatch => {

        dispatch(beginAjaxCall());
        axios.post(config.BASE_URL + '/flyer/settings', {id: flyer.id, settings: flyer.settings})
            .then(function (response) {

                dispatch({type: types.SAVE_SETTINGS_SUCCESS, response})

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });


    }

}

export const changeSetting = (setting) => {

    // console.log('change setting', setting);

    return (dispatch) => {
        dispatch({type: types.CHANGE_SETTING, setting})

    }
}