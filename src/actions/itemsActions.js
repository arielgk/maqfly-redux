import * as types from "./actionTypes";
import * as config from "../config";

import axios from 'axios';

import {beginAjaxCall, ajaxCallError} from "./ajaxStatusActions";

export const listItems = (flyerId) => {

    return (dispatch) => {

        dispatch(beginAjaxCall());

        axios.get(config.BASE_URL + '/parts/' + flyerId)
            .then(function (response) {

                dispatch({type: types.LIST_ITEMS_SUCCESS, items: response.data});

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });
    }
};

export const addItem = (item) => {
    return (dispatch) => {
        dispatch(beginAjaxCall());

        const type = item.name;
        const order = item.order;
        const flyer_id = item.flyer_id;
        const value = JSON.stringify(item.options);

        axios.post(config.BASE_URL + '/part', {
            name: type,
            type,
            order,
            flyer_id,
            value,
        })
            .then(function (response) {

                dispatch({type: types.ADD_ITEM_SUCCESS, item: response.data});

            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }
};

export const deleteItem = (index, id) => {

    return (dispatch) => {

        dispatch(beginAjaxCall());

        axios.delete(config.BASE_URL + '/part/' + id)
            .then(function (response) {

                dispatch({type: types.DELETE_ITEM_SUCCESS, index});
            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }
};

export const setActiveItem = (index) => {

    return (dispatch) => {

        dispatch({type: types.SET_ACTIVE_ITEM, index});
        dispatch({type: types.SET_ACTIVE_TAB, index: 1});

    }
};

export const unsetActiveItem = () => {
    return (dispatch) => {

        dispatch({type: types.UNSET_ACTIVE_ITEM});

    }
};
export const orderItems = (from, to) => {
    return {type: types.ORDER_ITEMS, order: {from, to}}
};

export const saveOrderItems = (items) => {

    return (dispatch) => {
        dispatch(beginAjaxCall());

        axios.post(config.BASE_URL + '/parts/order', {items: items})
            .then(function (response) {
                console.log('order-saved');
            })
            .catch(function (error) {
                console.log('order-error');
                dispatch(ajaxCallError());
            });
    }
}

export const saveOptions = (activeItem) => {
    return (dispatch, getState) => {

        const option2 = getState().items.filter((i, index) => {
            return i.id === activeItem;
        })[0];

        dispatch(beginAjaxCall());
        axios.post(config.BASE_URL + '/parts/save', option2)
            .then(function (response) {

                dispatch({type: types.SAVE_OPTIONS_SUCCESS});
            })
            .catch(function (error) {
                console.log(error);
                dispatch(ajaxCallError());
            });

    }

};

export const changeOption = (activeItem, option, save) => {
    return {type: types.CHANGE_OPTION, payload: {activeItem: activeItem, option: option}}

};

export const setActiveTab = (index) => {
    return {type: types.SET_ACTIVE_TAB, index: index}
};
