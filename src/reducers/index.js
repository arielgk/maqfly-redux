import {combineReducers} from 'redux'

import ajaxCallInProgress from './ajaxStatusReducer';

import items from './itemsReducer';
import compos from './composReducer';
import activeItem from './activeItemReducer';
import flyers from './flyersReducer';
import maqfly from './maqflyReducer';
import templates from './templatesReducer';
import activeTab from './tabsReducer';
import styles from './stylesReducer';
import settings from './settingsReducer';

const rootReducers = combineReducers({
    items,
    compos,
    activeItem,
    ajaxCallInProgress,
    flyers,
    maqfly,
    templates,
    activeTab,
    styles,
    settings,

});

export default rootReducers
