import * as types from '../actions/actionTypes';
import initialState from '../store/initialState';

const activeItem = (state = initialState.activeItem, action) => {
    switch (action.type) {
        case  types.DELETE_ITEM_SUCCESS:
            return 9999
        case types.SET_ACTIVE_ITEM:
            return action.index
        case types.UNSET_ACTIVE_ITEM:
            return 9999
        //in case of settings tabs default to 9999,
        case types.SET_ACTIVE_TAB:
            if (action.index === 2) {
                return 9999
            }
            return state

        default:
            return state
    }
}

export default activeItem;
