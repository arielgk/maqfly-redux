import initialState from '../store/initialState';
import * as types from "../actions/actionTypes";

const flyers = (state = initialState.flyers, action) => {
    switch (action.type) {
        case types.ADD_FLYER_SUCCESS:
            return [...state, action.flyer]
        case types.GET_FLYER_SUCCESS:
            return [...state, action.flyer]
        case types.GET_FLYERS_SUCCESS:
            return action.flyers
        default:
            return state
    }
}

export default flyers;
