import * as types from '../actions/actionTypes';
import initialState from '../store/initialState';
import {cloneDeep} from 'lodash';

const items = (state = initialState.items, action) => {
    switch (action.type) {

        case types.LIST_ITEMS_SUCCESS:
            return action.items;
        case types.ADD_ITEM_SUCCESS:

            return [...state, action.item]

        case types.DELETE_ITEM_SUCCESS:
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index + 1)
            ]
        case types.ORDER_ITEMS:
            const result = state;
            const [removed] = result.splice(action.order.from, 1);
            result.splice(action.order.to, 0, removed);
            return result;

        case types.CHANGE_STYLE_PROPERTY:
            let itemState = cloneDeep(state);
            return itemState.map((item, itemIndex) => {
                if (item.id === action.payload.activeItem) {
                    let editedItem = item;
                    editedItem.options = item.options.map((option, optionIndex) => {
                        let editedOption = option;
                        if (option.params && Object.keys(option.params).length > 0 && optionIndex === action.payload.index) {
                            editedOption.params.styles = Object.assign(option.params.styles,
                                {[action.payload.property]: action.payload.value});
                            return editedOption
                        } else {
                            return option;
                        }
                    });
                    return editedItem;
                } else {
                    return item
                }
            });

        case types.CHANGE_OPTION:
            let itsState = cloneDeep(state);
            return itsState.map((item, itemIndex) => {
                if (item.id === action.payload.activeItem) {
                    let editedItem = item;
                    editedItem.options = item.options.map((o) => {
                        if (o.key === action.payload.option.key) {
                            return action.payload.option
                        } else {
                            return o
                        }
                    });
                    return editedItem;
                } else {
                    return item
                }

            });

        default:
            return state
    }
}

export default items;
