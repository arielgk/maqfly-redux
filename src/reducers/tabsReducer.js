import * as types from '../actions/actionTypes';
import initialState from '../store/initialState';

const activeTab = (state = initialState.activeTab, action) => {

    switch (action.type) {
        case types.SET_ACTIVE_TAB:
            return action.index
        default:
            return state
    }
}

export default activeTab;
