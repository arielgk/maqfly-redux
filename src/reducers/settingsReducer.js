import initialState from '../store/initialState';
import * as types from "../actions/actionTypes";
import {cloneDeep} from 'lodash';

const settings = (state = initialState.settings, action) => {
    switch (action.type) {

        case types.CHANGE_SETTING:
            // should clonedeep becouse params property is an array
            let settingsState = cloneDeep(state);
            return settingsState.map((setting, index) => {
                if (setting.key === action.setting.key) {
                    return action.setting
                } else {
                    return setting
                }
            });

        case types.GET_FLYER_SUCCESS:
            return action.flyer.settings;
        default:
            return state
    }
}

export default settings;
